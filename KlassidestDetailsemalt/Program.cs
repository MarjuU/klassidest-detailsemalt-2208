﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassidestDetailsemalt
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Olen Ohhoo Main");

            Inimene i = new Inimene("34343434343")
            {
                Nimi = "mina",
                Vanus = 12
            };

            Console.WriteLine(i.Palk); // instantsi väli - pöördun konkreetse inimese palga poole
            Inimene.MiinimumPalk = 800; // static väli - pöördun kõigile inimestele ühise miinimumpalga poole, seda ei saa määrata igal objektil eraldi. 
            Console.WriteLine((new Inimene("48734343434")).Palk);

            Inimene uusNägu; // hetkel muutujal ei ole 
            uusNägu = null; // muutuja väärtus on kandiline null

            //Kolm erinevat võimalust luua uus objekt ja anda tema väljadele väärtused
            uusNägu = new Inimene("38989898989");
            uusNägu.Nimi = "Joosep";

            uusNägu = new Inimene("421199776767") { Nimi = "Karla" };

            uusNägu = new Inimene("34545678989") { Nimi = "Feeliks" };

            Inimene[] raffas = new Inimene[5];

            foreach (var x in Inimene.Inimesed)
            {
                Console.WriteLine(x.Nimi);
            }

            Console.WriteLine(Inimene.Inimesed[0]);

        }

        public class Inimene // kui siin ees on sõna public, siis saab teises projektis kasutada seda klassi viidates namespacele või pan,nes üles "using"
        {
            //loodud staatiline muutuja inimeste listi, kuhu ppärast paneme kõik loodud inimesed kirja
            public static List<Inimene> Inimesed = new List<Inimene>();

            static int inimesi = 0;

            static decimal miinimumPalk = 400; // kuna see on static, siis ta on kõigil klassis olevatel objektidel ühine
            public static decimal MiinimumPalk
            {
                get => miinimumPalk;
                internal set => miinimumPalk = Math.Max(miinimumPalk, value); // võtab kahest suurema ja paneb väärtuseks. internal teeb, et ainult sama projekti seest saab muuta
                // kui paneks internal, siis saab muuta ainult selle sama klassi seest
            }

            int number = ++inimesi; //järjekorranumber

            public readonly string IK; //isikukood, mida saab muuta ainult konstruktori sees
            public string Nimi; // näha ka teistes projektides (aga klassi poole peab pöörduma ikka "pika nimega")
            internal int Vanus; // aga vanust teises projektis kasutades ei näe, sest see on internal
            private decimal palk = MiinimumPalk;

            public decimal Palk // see on property, seda on vaja selleks, et välja muutmine oleks kontrollitud
            {
                get => palk; //get funktsioon väljastamiseks
                set => palk = value > palk ? value : palk; // set meetod palga muutmiseks
            }

            //loome konstruktori. Konstruktorit ei saa eraldi välja kutsuda. See kutsutakse välja automaatselt, kui programmis kasutatakse väljendit "new Inimene"
            //kui klassile pole loodud eraldi konstruktorit, tehakse vaikimisi konstruktor, millel pole sisu.
            //nagu meetod, mis on sama nimega, mis on klassi enda nimi. Meetodil käib void vahele, sellel ei ole. 
            public Inimene(string ik) // vaikeväärtuse nullid isikukoodi võib panna siia ka, siis ei ole vaja overloadimist teise konstruktoriga teha
            {
                this.IK = ik;
                Inimesed.Add(this); // this tähistab siin seda, kes loodi "new" väljendiga
                
            }

            public Inimene() : this("00000000000") // parameetritega inimese loomisel pannakse isikukoodi nullid. See on konstruktorite jadastamine
            { }

            public override string ToString() => $"{number} . {Nimi}, isikukoodiga {IK}";
            


            
        }
    }
}
